#include <stdio.h>
#include "DriverFactory.hpp"
#include <thread>
#include <VRDriver.hpp>
#include <sstream>
int main()
{
    int return_code = 0;
    void* ptr = HmdDriverFactory(vr::IServerTrackedDeviceProvider_Version, &return_code);
    vr::IVRDriverContext* pDriverContext = nullptr;
    vr::IServerTrackedDeviceProvider* driver = (vr::IServerTrackedDeviceProvider*) ptr;
    driver->Init(pDriverContext);

    while(1)
    {
        driver->RunFrame();
        usleep(1000);
    }

}
